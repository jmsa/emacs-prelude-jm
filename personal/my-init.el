(setq prelude-guru nil)
(setq x-select-enable-clipboard t)
(setq-default bidi-display-reordering nil)
(add-to-list 'load-path "~/.emacs.d/site-lisp/lookup2")
(add-to-list 'load-path "~/.emacs.d/site-lisp")


;; load yasnippet
;(require 'yasnippet)
;;(add-to-list 'yas-snippet-dirs prelude-snippets-dir)
;;(add-to-list 'yas-snippet-dirs prelude-personal-snippets-dir)
(yas-global-mode 1)

;; term-mode does not play well with yasnippet
;(add-hook 'term-mode-hook (lambda ()
;                            (yas-minor-mode -1)))



;(require 'adaptive-wrap)
(add-hook 'visual-line-mode-hook 'adaptive-wrap-prefix-mode

(autoload 'cobol-mode "cobol-mode" "Major mode for highlighting COBOL files." t nil)

;; To automatically laod cobol-mode.el upon opening COBOL files, add this:
 (setq auto-mode-alist
    (append
      '(("\\.cob\\'" . cobol-mode)
        ("\\.cbl\\'" . cobol-mode)
        ("\\.cpy\\'" . cobol-mode))
     auto-mode-alist))
)
;disable annoyin magit 1.4.0 message about data loss
(setq magit-last-seen-setup-instructions "1.4.0")
;custom faces
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 90 :width normal))))
 '(hl-line ((t (:inherit highlight :background "black"))))
 '(mode-line ((t (:background "#2B2B2B" :foreground "#8FB28F" :box (:line-width -1 :style released-button) :height 75))))
 '(region ((t (:background "dark violet")))))
 ;; "#3F3F3F"
