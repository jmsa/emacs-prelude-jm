;; definir función pantalla completa y asignarla a la tecla f11, como todo programa que se precie de llamarse así


(defun toggle-fullscreen ()
  "Toggle full screen on X11"
  (interactive)
  (when (eq window-system 'x)
    (set-frame-parameter
     nil 'fullscreen
     (when (not (frame-parameter nil 'fullscreen)) 'fullboth))))


;; toggle menu-bar visibility
;(global-set-key (kbd "S-<f10>") 'menu-bar-mode)

;(global-set-key [f11] 'toggle-fullscreen)
(global-set-key (kbd "C-z") 'undo-tree-undo) ; Ctrl+z
(global-set-key (kbd "C-S-z") 'undo-tree-redo) ;  Ctrl+Shift+z
(global-set-key (kbd "s-<prior>") 'previous-buffer) ;Super+RePág
(global-set-key (kbd "s-<next>") 'next-buffer) ;Super+AvPág
(global-set-key (kbd "s-<right>") 'windmove-right) ;Super+derecha
(global-set-key (kbd "s-<left>") 'windmove-left) ;Super+izquierda
(global-set-key (kbd "s-<down>") 'windmove-down) ;Super+derecha
(global-set-key (kbd "s-<up>") 'windmove-up) ;Super+izquierda

(global-set-key (kbd "s-SPC") 'company-complete-common) ; Super+espacio
;; use hippie-expand instead of dabbrev
(global-set-key (kbd "<f5>") 'hippie-expand)

(provide 'my-init-keys)

